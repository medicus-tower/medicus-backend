NUM_ROWS = 30
NUM_COLS = 30


class Position:
    """A simple Position class.

    >>> Position(1,2)
    Position(1, 2)

    >>> Position("1|2")
    Position(1, 2)

    >>> p = Position(4,3)
    >>> p + Position.directions["n"]
    Position(3, 3)

    >>> p + Position.directions["ne"]
    Position(3, 4)
    """
    directions = {
        'e': (0, 1),
        's': (1, 0),
        'w': (0, -1),
        'n': (-1, 0),
        'ne': (-1, 1),
        'se': (1, 1),
        'sw': (1, -1),
        'nw': (-1, -1)}

    def __init__(self, row, col=None):
        if col is None:
            row, col = row.split("|")
            row = int(row)
            col = int(col)
        self.row = row
        self.col = col

    @property
    def as_tuple(self):
        """
        >>> Position(4,3).as_tuple
        (4, 3)
        """
        return (self.row, self.col)

    @property
    def distance(self):
        """
        >>> Position(0,4).distance
        4.0

        >>> Position(4,4).distance
        6.0

        >>> Position(4,3).distance
        5.5

        >>> Position(4,2).distance
        5.0

        >>> Position(4,1).distance
        4.5

        >>> Position(4,0).distance
        4.0
        """
        max_ = max(map(abs, self.as_tuple))
        min_ = min(map(abs, self.as_tuple))
        return (max_ - min_) + min_ * 1.5

    def __hash__(self):
        """
        >>> hash(Position(4,3)) == hash(Position("4|3"))
        True

        """
        return hash(str(self))

    def __str__(self):
        """
        >>> str(Position(3,4))
        '3|4'
        """
        return "{}|{}".format(self.row, self.col)

    def __repr__(self):
        """
        >>> Position(1,2)
        Position(1, 2)
        """
        return "{}({}, {})".format(self.__class__.__name__, self.row, self.col)

    def __add__(self, other):
        """
        >>> Position(1,2) + Position(4,7)
        Position(5, 9)
        >>> Position(1,2) + (4,7)
        Position(5, 9)
        """
        if not isinstance(other, Position):
            other = Position(*other)
        return self.__class__(self.row + other.row, self.col + other.col)

    def __iadd__(self, other):
        """
        >>> p = Position(4,3)
        >>> p += (4,4)
        >>> p.as_tuple
        (8, 7)
        >>> p += Position(-1,4)
        >>> p
        Position(7, 11)
        """
        if isinstance(other, Position):
            row = other.row
            col = other.col
        else:
            row, col = other
        self.row += row
        self.col += col
        return self

    def __sub__(self, other):
        """
        >>> Position(4,7) - Position(1,2)
        Position(3, 5)
        >>> Position(4,7) - (1,2)
        Position(3, 5)
        """
        if not isinstance(other, Position):
            other = Position(*other)
        return self.__class__(self.row - other.row, self.col - other.col)

    def __isub__(self, other):
        """
        >>> p = Position(8,7)
        >>> p -= (4,4)
        >>> p.as_tuple
        (4, 3)
        >>> p -= Position(4,4)
        >>> p
        Position(0, -1)
        """
        if isinstance(other, Position):
            row = other.row
            col = other.col
        else:
            row, col = other
        self.row -= row
        self.col -= col
        return self

    def __neg__(self):
        """
        >>> - Position(1,2)
        Position(-1, -2)
        """
        return self.__class__(-self.row, -self.col)

    def __eq__(self, other):
        """
        >>> Position(3,9) == Position("3|9")
        True
        """
        return isinstance(other, Position) and self.row == other.row and self.col == other.col


class Map:

    def __init__(self, size=None):
        self._num_rows = size[0] if size else NUM_ROWS
        self._num_cols = size[1] if size else NUM_COLS
        self.tiles = {}

        # Build the map
        for row_idx in range(self._num_rows):
            for col_idx in range(self._num_cols):
                pos = Position(row_idx, col_idx)
                self.tiles[str(pos)] = Tile(pos)

        # generate neighbours
        for pos_str, tile in self.tiles.items():
            for direction in Position.directions.values():
                possible_neighbour = tile.pos + direction
                if str(possible_neighbour) in self.tiles:
                    tile.add_neighbour(self.tiles[str(possible_neighbour)])

        self.creeps = []
        self.towers = []

    def add_creep(self, creep):
        self.creeps.append(creep)

    def clean_creeps(self):
        self.creeps = [c for c in self.map.creeps if c.is_alive()]


class Tile:
    creeps_can_walk = True
    towers_can_be_build = True

    def __init__(self, pos):
        self.pos = pos
        self.neighbours = set()

    def is_able_to_link(self, other):
        """
        >>> t1 = Tile(Position(4,1))
        >>> t2 = Tile(Position(5,2))
        >>> t3 = Tile(Position(5,3))
        >>> t1.is_able_to_link(t2)
        True
        >>> t2.is_able_to_link(t1)
        True
        >>> t1.is_able_to_link(t3)
        False
        """
        if (self.pos - other.pos).distance <= 1.5:
            return True
        return False

    def add_neighbour(self, other):
        """
        >>> t1 = Tile(Position(4,1))
        >>> t2 = Tile(Position(5,2))
        >>> t3 = Tile(Position(5,3))
        >>> t1.add_neighbour(t2)
        >>> t1 in t2.neighbours
        True
        >>> t2 in t1.neighbours
        True
        >>> t1.add_neighbour(t3)
        >>> t1 in t3.neighbours
        False
        >>> t3 in t1.neighbours
        False
        >>> t3.add_neighbour(t2)
        >>> t3 in t2.neighbours
        True
        >>> t2 in t3.neighbours
        True
        """
        if self.is_able_to_link(other) and other.is_able_to_link(self):
            self.neighbours.add(other)
            other.neighbours.add(self)
