from medicore.game import Game
from unittest.mock import Mock, MagicMock, call


def test_add_player():
    """
    game.add_player returns a game_inform function that will call, game._inform_game but with the from_id set.

    game.inform_player will call the corresponding communicator.send function
    """
    game = Game()
    communicator = Mock()
    communicator.send = MagicMock()
    communicator_p2 = Mock()
    communicator_p2.send = MagicMock()
    inform_game_function = game.add_player(1, communicator)
    game.add_player(2, communicator_p2)
    assert communicator.send.called == False
    assert communicator_p2.send.called == False
    game.inform_player(1, "msg")
    assert communicator.send.called
    assert communicator_p2.send.called == False
    communicator.send.assert_called_with("msg")


    communicator.send.reset_mock()
    communicator_p2.send.reset_mock()
    game.inform_players("circular")
    assert communicator.send.called
    assert communicator_p2.send.called
    communicator.send.assert_called_with("circular")
    communicator_p2.send.assert_called_with("circular")

    game._inform_game = MagicMock()

    inform_game_function("title", "msg")
    # when inform_game_function is called, the _inform_game function is called,
    # with the from_id that was set by the add_player function
    game._inform_game.assert_called_with("title", "msg", from_id=1)
