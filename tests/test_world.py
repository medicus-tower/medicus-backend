import doctest
from medicore import world


def test_doctests():
	test_result = doctest.testmod(world)
	assert test_result.failed == 0
