from .world import Map
from collections import defaultdict


class Game:

    def __init__(self,):
        self.map = Map()
        self.player = defaultdict(list)
        self.tick_count = 0

    def add_player(self, player_id, communicator):
        # test if player_id is valid, but where
        if communicator not in self.player[player_id]:
            self.player[player_id].append(communicator)

        def inform_game_func(msg_type, msg):
            # we make sure, that the from_id can not be faked
            self._inform_game(msg_type, msg, from_id=player_id)

        return inform_game_func

    def _inform_game(self, msg_type, msg, from_id):
        """_inform_game(msg_type, msg, from_id)

        player communicators can send msgs to the game via this function. You should not use this by itself.
        the communicator can ask for this function via `add_player`.
        """

        inform_game_handler = self.get_inform_game_handler(msg_type)

        try:
            ret_val = inform_game_handler(msg, from_id)
        except Exception as e:
            ret_val = e

        ret_msg = self.generate_answer_msg(from_id, ret_val, msg_type, msg)
        if ret_msg:
            if hasattr(inform_game_handler, "inform_all_players") and inform_game_handler.inform_all_players:
                self.inform_players(ret_msg)
            else:
                self.inform_player(from_id, ret_msg)

    def inform_player(self, to_id, msg):
        for communicator in self.player[to_id]:
            communicator.send(msg)

    def inform_players(self, msg):
        for player_id in self.player.keys():
            # should this be inlined ???
            self.inform_player(player_id, msg)

    def generate_answer_msg(self, from_id, ret_val, msg_type, orig_msg):
        if isinstance(ret_val, dict):
            msg = ret_val
            msg['from_id'] = from_id
        else:
            if isinstance(ret_val, Exception):
                ret_val = str(ret_val)
            msg = dict(
                type="Ack",
                msg_type=msg_type,
                orig_msg=orig_msg,
                from_id=from_id,
                ret_val=ret_val,
            )

    def get_inform_game_handler(self, msg_type):
        return getattr(self, "{}_handler".format(msg_type), self.nope_handler)

    def nope_handler(self, msg, from_id):
        return None

    def tick(self):
        self.tick_count += 1
        creep_infos = []
        # spawn new creeps
        for creep in self.creep_spawner.tick(self.tick_count):
            self.map.add_creep(creep)

        # advance creeps
        for creep in self.map.creeps:
            info = creep.tick(self.tick_count)
            if info is not None:
                creep_infos.append(info)

        # clean dead creeps
        self.map.clean_creeps()

        tower_infos = []
        for tower in self.map.towers:
            info = tower.tick(self.tick_count)
            if info is not None:
                tower_infos.append(info)

        if creep_infos or tower_infos:
            self.inform_players(dict(msg_type="tick_info", creep_infos=creep_infos, tower_infos=tower_infos))
